import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div>
      <header>
        <div className="red">

          <h1 className="headerr">MovieScape</h1>
          <nav className="Navigation">
            <ul id="items">
              <li className="item">
                <a onclick="window.location.href='index.html';">Home</a>
              </li>
              <li className="item"><a href="./myBookings.html">
                My bookings
              </a>
              </li>
              <li className="item">
                <a href="./contactUs.html">Contact us</a>
              </li>
              <li className="item" id="y">
                <a href="./about.html">About</a>
              </li>
            </ul>
          </nav>

        </div>
      </header>

      <main>
        <div className="main_container" id="movies">
        </div>
      </main>
      
      <footer>
        <p>copyright &#169; @sicxbit</p>
      </footer>

      <script src="./index.js" type="module"></script>
    </div>
  );
}

export default App;
