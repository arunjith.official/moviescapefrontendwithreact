import Controller from "../controller/admin-controller.js"

const controller = new Controller ();
const bookingsDiv = document.getElementById("bookingsDiv")
const allBookings = document.getElementById("showAllBookings");
const accounts = document.getElementById("accountsDiv");
const movieForm = document.getElementById("movieForm");
const screenform = document.getElementById("screenForm");
const screeningForm = document.getElementById("screeningForm")

const screen_name = document.getElementById("screenName");
const total_seats = document.getElementById("totalSeats");
const screen_description = document.getElementById("screenDescription");

const movie_name = document.getElementById("movieName");
const movie_duration = document.getElementById("movieDuration");
const release_date = document.getElementById("releaseDate");
const movieButton = document.getElementById("movieButton");
const usersButton = document.getElementById("usersButton");
const movie_description = document.getElementById("movieDescription");

const movie_id = document.getElementById("movieID")
const screen_id = document.getElementById("screenID")
const show_time = document.getElementById("showTime")
screenform.addEventListener("submit", (e)=> {
    e.preventDefault()
    createNewScreen(screen_name.value,total_seats.value,
        screen_description.value);
    console.log(screen_name.value,total_seats.value,
        screen_description.value);
})
function createNewScreen(screen_name,total_seats,screen_description) {
    console.log(screen_name,total_seats,screen_description);
    controller.createScreen(screen_name,total_seats,
        screen_description).then((r) => {
        alert(`${screen_name} added successfully`)
        screenform.reset()
        })
}

movieForm.addEventListener("click", (e) => {
    e.preventDefault()
    createNewMovie(movie_name.value,
        movie_duration.value,release_date.value,
        movie_description.value);
        console.log(movie_name.value)
        console.log(movie_description.value);
})

function createNewMovie(movie_name,movie_duration,release_date,movie_description){
    controller.createMovie(movie_name,movie_duration,
        release_date,movie_description).then ((r)=> {
            alert(`${movie_name} added successfully`)
            movieForm.reset()
        })
}

screeningForm.addEventListener("submit", (a) => {
    a.preventDefault()
    createNewScreening(movie_id.value,screen_id.value,
        show_time.value)
    console.log(movie_id.value,screen_id.value,show_time.value)
})

function createNewScreening(movie_id,screen_id,show_time) {
    controller.createScreening(movie_id,screen_id,
        show_time).then((ee) => {
            alert("screening has been created")
            screeningForm.reset()
        })
}


movieButton.addEventListener("click" ,(a) => {
    renderAllseats()
})

function renderAllseats() {
    movieButton.remove()
    controller.getBookings().then((data) => {
        const bookings = data
        console.log(bookings)
        for(let i=0;i<bookings.length;i++) {
            let booking= bookings[i]
            const boxDiv = document.createElement('div')
            boxDiv.classList.add('bookings')
            boxDiv.innerHTML = `
            <span>Booking ID: ${booking.booking_id}</span>
            <span>Booked By: ${booking.booked_by}</span>
            <span>Phone: ${booking.phone}</span>
            <span>Show ID: ${booking.show_id}</span>
            `
            bookingsDiv.append(boxDiv)
        }
    })    
}

usersButton.addEventListener("click" ,(a) => {
    renderUsers()
})

async function renderUsers() {
    usersButton.remove()
    accounts.style.height = "auto";
    await controller.getUsers().then((data) => {
        console.log(data)
        for(let i=0;i<data.length;i++) {
            let users= data[i]
            const boxDiv = document.createElement('div')
            boxDiv.classList.add('users')
            boxDiv.innerHTML = `
            <span>Username: ${users.username}</span>
            <br>
            <span>Password: ${users.password}</span>
            `
            accounts.append(boxDiv)
        }
    })
}