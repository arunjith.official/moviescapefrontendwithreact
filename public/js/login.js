// import Controller from "../controller/login-Controller.js";

// const controller = new Controller ();


const form = document.getElementById("form");
const createAccount = document.getElementById("createAccount");
const login = document.getElementById("login");
const username = document.getElementById("username");
const password = document.getElementById("password");
const accountForm = document.getElementById("createAccountForm");


newAccount.addEventListener("click", (e) => {
    e.preventDefault()
    renderNewAccInput()
})


function renderNewAccInput() {
    login.remove()
    newAccount.remove()
    const boxDiv = document.createElement('div')
    boxDiv.classList.add('AccountForm')
    boxDiv.innerHTML = `
    <div id="createAccountForm">
    <h4 class="form_header">Sign in</h4>
    <br>
    <label for="newUsername">New username</label>
    <br>
    <input type="text" id="newUsername" name="username">
    <br>
    <div id="messageUser">
    </div>
    <label for="password">Your Password</label>
    <br>
    <input type="password" id="password" name="password">
    <br>
    <div id="passwordMessage">
    </div>
    <label for="confirmPassword">Confirm Password</label>
    <br>
    <input type="password" id="confirmPassword" name="confirm_password">
    <br>
    <div id="alert">
    </div>
    <div class="buttonDiv">
    <br>
    <button type="submit">Create account</button>
    </div>`
    form.append(boxDiv)

    const newUsername = document.getElementById("newUsername");
    const password = document.getElementById("password");
    const confirmPassword = document.getElementById("confirmPassword");
    const messageDiv = document.getElementById("alert")
    const messageUser = document.getElementById("messageUser")
    const passwordMessage = document.getElementById("passwordMessage");

    form.addEventListener("submit", (e) => {
        e.preventDefault()
        saveUsernamePassword()
    })
    
    function saveUsernamePassword() {
        clearContent(messageDiv)
        clearContent(messageUser)
        clearContent(passwordMessage)
        function clearContent(element) {
            element.innerHTML=``
        }
        if ((newUsername.value.length) != 0) {
            if (password.value === confirmPassword.value){
                console.log("proceed")
            }
            else if (password.value.length === 0) {
                const message = document.createElement("div")
                message.innerHTML=`<span>password cannot be empty</span>`
                passwordMessage.append(message)
            }
            else if (confirmPassword.value.length === 0){
                const message = document.createElement("div")
                message.innerHTML=`<span>password cannot be empty</span>`
                messageDiv.append(message)
            }
            else if (password.value !== confirmPassword.value){
                const message = document.createElement("div")
                message.innerHTML=`<span>passwords doesnt match</span>`
                messageDiv.append(message)
            }
        }
        else if (newUsername.value.length === 0 ) {
            const message = document.createElement("div")
            message.innerHTML = `<span>username cannot be empty</span>`
            messageUser.append(message)
        }
    }
}

